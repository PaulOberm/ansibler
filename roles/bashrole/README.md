Role Name
=========

This role configures bash on Linux based OS.


Requirements
------------
Use `requirements.txt`
To test, run in virtual environment and use `molecule create`, `molecule converge`, `molecule list`, `molecule verify` and `molecule destroy`
or use `molecule test`.
To check within the container use `molecule login`.


Creation
--------
- `molecule init role acme.bashrole --driver-name docker`


Role Variables
--------------

Dependencies
------------


Example Playbook
----------------


License
-------

BSD


Author Information
------------------

Paul OBERMAYER paul.obermayer@gmail.com
