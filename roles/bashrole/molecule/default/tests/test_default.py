import warnings
with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    import os
    import testinfra.utils.ansible_runner
    # EXAMPLE_1: make the linter fail by importing an unused module
    # Hint: From now on, try running molecule test --destroy never
    import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

def test_username_folder(host):
    # Asserts that there is a username defined in vars int /etc
    f = host.file('/home')
    folders = f.listdir()

    assert "obermpa" in folders

def test_basrc_exists(host):
    f = host.file('/home/obermpa')
    user_content = f.listdir()

    # assert ".bashrc" in user_content

# def test_xfce_settings(host):
#     f = host.file('/home/obermpa/.config/xfce4/xfconf/xfce-perchannel-xml/')
#     xfce_content = f.listdir()

#     assert "xfce4-keyboard-shortcuts.xml" in xfce_content
