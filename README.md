# Laptop configurator
This repository is used to bootstrap a new Linux instance with user defined settings.
For example installed packages, applications and configurations. 
This project is based on **Ansible**, **Ansible pull** and **Molecule**.
Ansible pull comes installed with Ansible. Molecule is the testing framework for Ansible.
This setup allows a machine to pull the repository from a Git server and deploy 
`local.yml` locally.
So whenever a new state is checked into the repository on the git server, 
the corresponding machine can pull it whenever it connects to the internet.
On top of that Molecule allows Test Driven Development and guarantees quality.
The combination of all of that generates a full CI/CD approach.


***

## Getting started
To use this repository, set up a new Linux machine and follow the [preparation steps](##preparation-steps)!
Afterwards follow the instructions in [how to deploy](##test-and-deploy).

***

## Important commands
- List instances molecule will test on: `molecule list`
- Provision container with Ansible: `molecule create`
- Ansible lint on role: `ansible-lint .`


***

## Preparation steps
- [ ] [Install a Linux based distribution](https://cdimage.debian.org/images/unofficial/non-free/images-including-firmware/current-live/amd64/iso-hybrid/)
Iso image on USB stick and install on machine. I used **debian-live-11.5.0-amd64-xfce+nonfree.iso**
- [ ] [Install Ansible](https://www.ansiblepilot.com/articles/how-to-install-ansible-in-debian-11/)
```
sudo apt update
sudo apt install ansible
which ansible-pull
```
- [ ] [Create ssh keypair](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-debian-11)
This keypair has to be available on the machine we want to work on. Call it the key pair **ansibler**
```
ssh-keygen
cd /home/USERNAME
cat ansibler.pub
```
- [ ] [Install ssh server on machine (only if private repo)](https://www.linuxcapable.com/install-enable-ssh-on-debian-11-bullseye/)
This is necessary to still connect from another machine to get public key back up to Git server in next step.
```
sudo apt install openssh-server
sudo systemctl enable --now ssh
systemctl status ssh
```
- [ ] [Create ssh key on git server (only if private repo)](https://www.tutorialspoint.com/gitlab/gitlab_ssh_key_setup.htm)
Get public key from remote machine
```
ssh rec@IPADDRESS
```
- [ ] [Clone repo and execute Playbook on machine](https://gitlab.com/PaulOberm/ansibler.git)
```
cd /home
sudo ansible-pull -U https://gitlab.com/PaulOberm/ansibler.git
```


***

## Test and Deploy
Start virtual environment:
```
source env/bin/activate
```
Use the built-in continuous integration in GitLab.
Within a pipeline the Playbook is tested.
To test on the machine run for example: 

```
htop --version
```


## Test locally
```
ansible-playbook main.yml --ask-become-pas
```

***

## Project status
Development whenever there is time and need.
[![pipeline status](https://gitlab.com/PaulOberm/ansibler/badges/main/pipeline.svg)](https://gitlab.com/PaulOberm/ansibler/-/commits/main)
